# Single user Docker image for Jupyterhub

* Automized builds on [dockerhub/quantumtinkerer/research](https://hub.docker.com/r/quantumtinkerer/research/)
* Official repo on [kwant-gitlab/qt/research-docker](https://gitlab.kwant-project.org/qt/research-docker)
* GitHub mirror on [github.com/quantum-tinkerer/research-docker](https://github.com/quantum-tinkerer/research-docker)
